#!/bin/sh

FUSESERVERLIST=("localhost" "192.168.33.35")
FUSE_HOME=/home/vagrant/jboss-fuse
DEPLOY_DIR=/vagrant

CMDNAME=$0
if [ $# -ne 1 ]; then
  echo "Usage: $CMDNAME <BUNDLEFILENAME>" 1>&2
  exit 1
fi

BUNDLEFILENAME=$1

for FUSESERVER in ${FUSESERVERLIST[@]}; do
    echo "Deploying to server ${FUSESERVER}"
    ${FUSE_HOME}/bin/client -h ${FUSESERVER} -a 8101 -u admin -p admin -r 3 -d 5 "
    osgi:install -s file:${DEPLOY_DIR}/${BUNDLEFILENAME}
    "
done
echo "Deploy finished."
