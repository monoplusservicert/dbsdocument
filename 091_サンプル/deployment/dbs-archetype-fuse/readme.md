## HOW TO USE THIS ARCHETYPE

You can generate demo1 application as below.
```sh
mvn archetype:generate                                 \
  -DarchetypeGroupId=com.monoplus.dbs.archetype.fuse   \
  -DarchetypeArtifactId=dbs-archetype-fuse             \
  -DarchetypeVersion=0.0.1-SNAPSHOT                    \
  -DgroupId=com.monoplus.dbs.sample                    \
  -DartifactId=demo1                                   \
  -Dversion=0.0.1-SNAPSHOT
```

After that, you can build and create report.
```sh
cd demo1
mvn test
mvn clean install
```


## HOW CREATED THIS ARCHETYPE

1. creating the directory structure needed for an archetype
```sh
mvn archetype:generate                             \
  -DgroupId=com.monoplus.dbs.archetype.fuse        \
  -DartifactId=dbs-archetype-fuse                  \
  -Dversion=0.0.1-SNAPSHOT                         \
  -DarchetypeArtifactId=maven-archetype-archetype
```

2. customize the contents of the archetype-resources directory

3. customize the contents of archetype-metadata.xml

4. install the archetype:

```sh
mvn clean install
```

5. try it on your local system by using the following command. In this command, you need to specify the full information about the archetype you want to use (its groupId, its artifactId, its version)
```sh
mvn archetype:generate                                 \
  -DarchetypeGroupId=com.monoplus.dbs.archetype.fuse   \
  -DarchetypeArtifactId=dbs-archetype-fuse             \
  -DarchetypeVersion=0.0.1-SNAPSHOT                    \
  -DgroupId=com.monoplus.dbs.sample                    \
  -DartifactId=demo1                                   \
  -Dversion=0.0.1-SNAPSHOT
```  
