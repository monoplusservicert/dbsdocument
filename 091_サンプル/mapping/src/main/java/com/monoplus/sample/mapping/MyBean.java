package com.monoplus.sample.mapping;

public class MyBean {
	public String funcA(String str) {
		System.out.println("funcA実行中  str=" + str);
		return "helloA(" + str + ")";
	}

	public String funcB(String str1, String str2) {
		System.out.println("funcB実行中  str1=" + str1 + "  ,   str2=" + str2);
		return "helloB(" + str1 + ", " + str2 + ")";
	}

	public String funcC(String str) {
		System.out.println("funcC実行中  str=" + str);
		return "helloC(" + str + ")";
	}

}
