package com.monoplus.sample.mapping;

public class TargetObj {

	private String out1;
	private String out2;
	private String out3;
	private String out4;
	private String out5;
	private String out6;
	private String out7;
	private String out8;
	private String out9;
	private String out10;
	
	@Override
	public String toString() {
		return "TargetObj [out1=" + out1 + ", out2=" + out2 + ", out3=" + out3
				+ ", out4=" + out4 + ", out5=" + out5 + ", out6=" + out6
				+ ", out7=" + out7 + ", out8=" + out8 + ", out9=" + out9
				+ ", out10=" + out10 + "]";
	}

	public String getOut1() {
		return out1;
	}
	public void setOut1(String out1) {
		this.out1 = out1;
	}
	public String getOut2() {
		return out2;
	}
	public void setOut2(String out2) {
		this.out2 = out2;
	}
	public String getOut3() {
		return out3;
	}
	public void setOut3(String out3) {
		this.out3 = out3;
	}
	public String getOut4() {
		return out4;
	}
	public void setOut4(String out4) {
		this.out4 = out4;
	}
	public String getOut5() {
		return out5;
	}
	public void setOut5(String out5) {
		this.out5 = out5;
	}
	public String getOut6() {
		return out6;
	}
	public void setOut6(String out6) {
		this.out6 = out6;
	}
	public String getOut7() {
		return out7;
	}
	public void setOut7(String out7) {
		this.out7 = out7;
	}
	public String getOut8() {
		return out8;
	}
	public void setOut8(String out8) {
		this.out8 = out8;
	}
	public String getOut9() {
		return out9;
	}
	public void setOut9(String out9) {
		this.out9 = out9;
	}
	public String getOut10() {
		return out10;
	}
	public void setOut10(String out10) {
		this.out10 = out10;
	}

}
