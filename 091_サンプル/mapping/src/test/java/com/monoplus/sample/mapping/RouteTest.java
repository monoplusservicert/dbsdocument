package com.monoplus.sample.mapping;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.EndpointInject;
import org.apache.camel.Produce;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.blueprint.CamelBlueprintTestSupport;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class RouteTest extends CamelBlueprintTestSupport {

	@Produce(uri = "direct:SampleMapping")
	private ProducerTemplate startEndpoint;

	@Override
	protected String getBlueprintDescriptor() {
		return "OSGI-INF/blueprint/camelContext.xml";
	}

	@Test
	public void testRoute() throws Exception {

		// set data to SourceObj
		SourceObj source = new SourceObj();
		final String input1 = "1.23";
		final String input2 = "4.56";
		source.setIn1(input1);
		source.setIn2(input2);

		// set data to exchangeHeaders
		Map<String, Object> exchangeHeaders = new HashMap<String, Object>();

		// prepare TargetOjb
		TargetObj target = new TargetObj();
		target = (TargetObj) startEndpoint.requestBodyAndHeaders(source,
				exchangeHeaders);

		// check results

		// 機能候補No.221 上入力数値が下入力数値より大きいかどうか判定する
		Assert.assertEquals("4.56", target.getOut1());
		// 機能候補No.235 2つの入力数値を足して返す
//		Assert.assertEquals("5.79", target.getOut2());
		// 機能候補No.240 切り上げ　入力数値以上で最も近い整数を返す
		Assert.assertEquals("2.0", target.getOut3());
		// 機能候補No.283 3文字目から4文字目までを切り取る
		Assert.assertEquals("23", target.getOut4());
		// 機能候補No.337 ユーザ定義ロジック（MyBean）
		Assert.assertEquals("1.23-helloA(4.56)", target.getOut5());
		
		
	}

}
